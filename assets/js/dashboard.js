$( document ).ready(function () {

    //-------- Add Product FORM ---------\\
    $('#add_product')
        .form({
            fields: {
                title: {
                    identifier: 'title',
                    rules: [{
                            type   : 'empty',
                            prompt : 'Titel mag niet leeg zijn'
                        },{
                            type   : 'minLength[4]',
                            prompt : 'Titel moet langer zijn dan 3 tekens'
                        }
                    ]
                },
                price: {
                    identifier: 'price',
                    rules: [{
                            type   : 'decimal',
                            prompt : 'Prijs moet een getal zijn.'
                        }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                $('.add-product-done-message').hide();
                $('.add-product-error-message').hide();
                $(this).addClass('loading');
                if (sendData($('.form').serialize() + '&add_product=true')){
                    $(':input','#add_product')
                        .not(':button, :submit, :reset, :hidden')
                        .val('');
                    $('.add-product-done-message').show();
                }
                $('#add_product')
                    .form('clear');
                $('.add-product-done-message').show();
                $(this).removeClass('loading');
            }
        });

    //-------- Add Admin FORM ---------\\
    $('#add_admin')
        .form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [{
                        type   : 'empty',
                        prompt : 'Email mag niet leeg zijn!'
                    },{
                        type   : 'email',
                        prompt : 'Email is niet correct!'
                    }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [{
                        type   : 'minLength[5]',
                        prompt : 'Wachtwoord moet minimaal 5 tekens lang zijn!'
                    }
                    ]
                },
                crypt_password: {
                    identifier: 'crypt_password',
                    rules: [{
                        type   : 'match[password]',
                        prompt : 'Wachtwoorden zijn niet gelijk!'
                    }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                $('.add-admin-done-message').hide();
                $('.add-admin-error-message').hide();
                $(this).addClass('loading');

                if (sendData($('.form').serialize() + '&add_admin=true')){
                    $(':input','#add_admin')
                        .not(':button, :submit, :reset, :hidden')
                        .val('');
                    $('.add-admin-done-message').show();
                }else{
                    if (data.includes('ALREADY_EXISTS')){
                        //TODO: Admin already exists
                    }
                    $('.add-admin-error-message').show();
                }

                $(this).removeClass('loading');
            }
        });

    //-------- Add Customer FORM ---------\\
    $('#add_customer')
        .form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [{
                        type   : 'empty',
                        prompt : 'Email mag niet leeg zijn!'
                    },{
                        type   : 'email',
                        prompt : 'Email is niet correct!'
                    }
                    ]
                },
                role: {
                    identifier  : 'role',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Selecteer een rol'
                        }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [{
                        type   : 'minLength[5]',
                        prompt : 'Wachtwoord moet minimaal 5 tekens lang zijn!'
                    }
                    ]
                },
                crypt_password: {
                    identifier: 'crypt_password',
                    rules: [{
                        type   : 'match[password]',
                        prompt : 'Wachtwoorden zijn niet gelijk!'
                    }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                $('.add-customer-done-message').hide();
                $('.add-customer-error-message').hide();
                $(this).addClass('loading');

                if (sendData($('.form').serialize() + '&add_customer=true')){
                    $(':input','#add_customer')
                        .not(':button, :submit, :reset, :hidden')
                        .val('');
                    $('.add-customer-done-message').show();
                }else{
                    if (data.includes('ALREADY_EXISTS')){
                        //TODO: Admin already exists
                    }
                    $('.add-customer-error-message').show();
                }

                $(this).removeClass('loading');
            }
        });

    //------------------ Extra's ----------------\\

    $('select.dropdown')
        .dropdown();

    //--------------------------------------------\\
});

function sendData(serialized) {
    $.ajax({
        type: "POST",
        data: serialized,
        url: "./base/dashboard_request.php",
        success:function(data) {
            // alert(data);
            return data.includes('SUCCES');
        }
    });
}