$( document ).ready(function () {

    $('#login-form, #order-login')
        .form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Email mag niet leeg zijn!'
                        },
                        {
                            type   : 'email',
                            prompt : 'Email is geen correcte email'
                        }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Wachtwoord mag niet leeg zijn!'
                        }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                $('.submit').addClass('loading');

                $('.submit, .login.input').addClass('disabled');
                $.ajax({
                    type: "POST",
                    data: $('.form').serialize() + '&USER_LOGIN=USER',
                    url: "./base/login_request.php",
                    success:function(data) {
                        if (data.includes('VALID_LOGIN')){
                            $('.login-form-succes-message').show();
                            location.reload();
                        }else {
                            $('.login-form-error-message').show();
                        }
                        $('.submit').removeClass('loading');
                        $('.submit, .login.input').removeClass('disabled');
                    }
                });
            }
        });

    //-------- Register Form ---------\\
    $('#register-form')
        .form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [{
                        type   : 'empty',
                        prompt : 'Email mag niet leeg zijn!'
                    },{
                        type   : 'email',
                        prompt : 'Email is niet correct!'
                    }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [{
                        type   : 'minLength[5]',
                        prompt : 'Wachtwoord moet minimaal 5 tekens lang zijn!'
                    }
                    ]
                },
                crypt_password: {
                    identifier: 'crypt_password',
                    rules: [{
                        type   : 'match[password]',
                        prompt : 'Wachtwoorden zijn niet gelijk!'
                    }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                $('.submit').addClass('loading');
                $('.submit, .register.input').addClass('disabled');

                if (sendData($(this).serialize() + '&add_customer=true')){
                    $('#register-form')
                        .form('clear');
                    $('.add-customer-done-message').show();
                }else{
                    $('.add-customer-error-message').show();
                }

                $(this).removeClass('loading');
            }
        });

    $('.ui.add_to_cart').click(function () {
        var div = $(this);
        var data = div.attr('product_id');
        var bool = div.attr('add_to_cart');

        div.addClass("loading");
        div.addClass("disabled");

        $.ajax({
            type: "POST",
            data: {
                id: data,
                add_to_cart: bool
            },
            url: "./base/webshop_request.php",
            success: function (data) {
                // alert(data);
                window.location.reload();
            }
        });

        div.removeClass('loading');
        div.removeClass('disabled');
    });

    $( '#sign-out' ).click(function () {
        $('.mini.modal.sign-out').modal({
            onApprove : function() {
                sign_out();
            }
        }).modal('show');
    });

    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade');
        });
    
    $('#login, #order_login').click(function () {
        $('.ui.modal.login')
            .modal('show');
    });


    //-------- Guest Login Order ---------\\
    $('#order-login').click(function (evt) {
        $('.submit').addClass('loading');
        $.ajax({
            type: "POST",
            data: 'QUEST_LOGIN=GUEST',
            url: "./base/login_request.php",
            success:function(data) {
                $('.submit').removeClass('loading');
                window.location.reload();
            }
        });
        evt.stopImmediatePropagation();
    });

    //------------------ Order Account ----------------\\
    $('#add_order')
        .form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Email mag niet leeg zijn!'
                        },
                        {
                            type   : 'email',
                            prompt : 'Email is geen correcte email'
                        }
                    ]
                },
                firstname: {
                    identifier: 'firstname',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Voornaam mag niet leeg zijn.'
                        }
                    ]
                },
                street: {
                    identifier: 'street',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Straat mag niet leeg zijn.'
                        }
                    ]
                }
                ,
                house_number: {
                    identifier: 'house_number',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Huisnummer mag niet leeg zijn.'
                        }
                    ]
                }
                ,
                zip: {
                    identifier: 'zip',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Postcode mag niet leeg zijn.'
                        }
                    ]
                }
                ,
                city: {
                    identifier: 'city',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Woonplaats mag niet leeg zijn.'
                        }
                    ]
                },
                terms: {
                    identifier: 'terms',
                    rules: [{
                        type   : 'checked',
                        prompt : 'Je moet akkoord gaan met de voorwaarden!'
                    }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [{
                        type   : 'minLength[5]',
                        prompt : 'Wachtwoord moet minimaal 5 tekens lang zijn!'
                    }
                    ]
                },
                crypt_password: {
                    identifier: 'crypt_password',
                    rules: [{
                        type   : 'match[password]',
                        prompt : 'Wachtwoorden zijn niet gelijk!'
                    }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();

                var div = $('#submit_order');
                var url = div.attr('url');

                $('#order_step_1').addClass('loading');
                $.ajax({
                    type: "POST",
                    data: $('.form').serialize() + '&ORDER_STEP_1=SUCCES',
                    url: "./base/order_request.php",
                    success:function(data) {
                        alert(data);
                        if (data.includes('SUCCES')){
                            location.href = url;
                        }else{
                            // alert(data);
                        }
                        $('#order_step_1').removeClass('loading');
                    }
                });
                event.stopImmediatePropagation();
            }
        });

    //------------------ Extra's ----------------\\

    $('select.dropdown')
        .dropdown();

    $('#huishouden').click(function () {
        $.ajax({
            type: "POST",
            data: 'EMAIL=EMAIL',
            url: "./base/order_request.php",
            success:function(data) {
                alert('succes =' + data);
            }
        });
    });

    $('#product-search').keyup(throttle(function() {
        $.ajax({
            type: "POST",
            data: 'FILTER='+$(this).val(),
            url: "./base/webshop_request.php",
            success:function(data) {
                $('#product-items').load(document.URL +  ' #product-items');
            }
        });
    }));

    //--------------------------------------------\\

});

function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 550);
    };
}

function sendData(serialized) {
    $.ajax({
        type: "POST",
        data: serialized,
        url: "./base/webshop_request.php",
        success:function(data) {
            // alert(data);
            return data.includes('SUCCES');
        }
    });
}

function sign_out(){
    $('.ui.loader.sign-out').show();
    setTimeout(function () {
        $.ajax({
            url : './base/dashboard_request.php',
            data: { "safe_sign_out" : true},
            type: "POST",
            success:function(data) {
                if (data.includes('SUCCES')){
                    $('.ui.loader.sign-out').hide();
                    location.reload();
                }
            }
        }).fail(function() {
            $('.ui.loader.sign-out').hide();
            error_404();
        });
    }, 1000);

}

function error_404(){
    //TODO: Show modal
}

// function loading_dimmer(){
//     $('body').dimmer({
//         closable: false,
//         template: {
//             dimmer: function() {
//                 return $('<div>').attr('class', 'ui dimmer active')
//                     .append('<div class="ui massive text loader">Bezig....</div>');
//             }
//         },
//     }).dimmer('show');
// }