var klantnaam = prompt("voer je eigen naam in");
var aantal = parseInt(prompt("aantal boeken te bestellen"));
var titel = "Javascript";
var prijs = 29.90;
var bedrag = aantal * prijs;
var btw = bedrag / 100 * 6;
var total = bedrag + btw;

document.write("Bedankt voor je bestelling: " + klantnaam);
document.write("<br> Boektitel is: " + titel);
document.write("<br> Aantal boeken te bestellen: " + aantal);
document.write("<br> Prijs per boek is: " + prijs.toFixed(2));
document.write("<br> Bedrag is: " + bedrag.toFixed(2));
document.write("<br> BTW: " + btw.toFixed(2));
document.write("<br> Totaal Inc. BTW: " + total.toFixed(2));
