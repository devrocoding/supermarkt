<?php
require_once '../classes/dash/Functions.php';

$functions = new Functions();

if (!isset($_GET[str_replace('=', '', $functions->short)])) {
    header("Location:". $_SERVER['PHP_SELF']  . '?' . $functions->short . $functions->serialize('cart'));
}

if (!$shop->hasAShoppingCart()) {
    header('Location: ../shopping_cart.php');
}

if (!$user->isLoggedIn()) {
    //TODO: Show no account
}


?>

<html>
<head>
    <?php include("../base/imports.php"); ?>
</head>
<body>

<content>
    <?php include_once '../sidebar.php'; ?>

    <div class="ui container" id="dashboard">
        <div class="ui grid">
            <div class="sixteen wide column">
                <?php $functions->readPage($functions->getPageFromCurrentUrl()); ?>
            </div>
        </div>
    </div>

    <?php include_once '../base/scripts.php'; ?>
    <script src="./assets/js/dashboard.js"></script>
</content>

</body>
</html>