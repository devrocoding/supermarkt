<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 12-12-2017
 * Time: 11:18
 */

require_once './classes/User.php';
require_once './classes/Shop.php';
require_once './classes/product.php';
$pr = new product();
$user = new User();
$shop = new Shop();
session_start();

?>


<html>
<head>
    <?php include("base/imports.php"); ?>
</head>
<body>

<content>
    <?php include_once 'sidebar.php';?>
    <div class="ui container">
        <div class="ui grid">
            <div class="sixteen wide column">
                <h1>Winkelwagen</h1>
                <?php if ($shop->getProductsInCart() != null){ ?>
                    <div class="ui relaxed divided items">
                        <?php foreach ($shop->getProductsInCart() as $product){ ?>
                            <div class="link item">
                                <div class="ui small image">
                                    <?php if ($product['image'] == null){ ?>
                                        <img src="./assets/img/no_img.png">
                                    <?php }else{ ?>
                                        <img src="./assets/img/products/<?php echo $product['image']; ?>">
                                    <?php } ?>
                                </div>
                                <div class="content">
                                    <a class="header"><?php echo $product['title']; ?></a>
                                    <div class="description">
                                        <?php echo $product['desc']; ?>
                                    </div>
                                    <div class="extra">
                                        <div class="ui tag labels">
                                            <br>
                                            <a class="ui label green">
                                                €<?php echo number_format($pr->getPrice($product), 2, ',', '.'); ?>
                                            </a>
<!--                                            <a class="ui label">-->
<!--                                                2 KG-->
<!--                                            </a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php } ?>
                    </div>
                    <div class="ui red segment">
                        <div class="ui grid">
                            <div class="eight wide column">
                                <h1>Bent u nog een product vergeten?</h1>
                            </div>
                            <div class="eight wide column right floated center aligned">
                                <table class="ui definition table">
                                    <tbody>
                                    <tr>
                                        <td>Subtotaal (Inc. BTW)</td>
                                        <td>€ <?php echo $shop->getTotalPriceCart(); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Totaal BTW</td>
                                        <td>€ <?php echo $shop->getVat(); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bezorgkosten</td>
                                        <?php if ($shop->getShippingPrice() <= 0){ ?>
                                            <td><b>GRATIS</b></td>
                                        <?php }else{ ?>
                                            <td>€ <?php echo $shop->getShippingPrice(); ?></td>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td class="positive">Totaal</td>
                                        <td class="positive"><b>€ <?php echo $shop->getTotal(); ?></b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="sixteen wide column"></div>
                        </div>
                        <div class="ui buttons fluid">
                            <a class="ui button" href="./shop">Verder Winkelen</a>
                            <div class="or"></div>
                            <button class="ui positive button" onclick="window.location = './order'">Bestellen</button>
                        </div>
                    </div>
                <?php }else{ ?>
                        <div class="ui icon error message">
                            <i class="attention circle icon"></i>
                            <div class="content">
                                <div class="header">Je hebt een lege winkelwagen</div>
                                <p>Je kan je winkelwagen niet bekijken omdat je nog niets erin hebt gestopt!</p>
                            </div>
                        </div>
                        <div class="ui fluid buttons">
                            <button class="ui button orange">Inloggen</button>
                            <div class="or"></div>
                            <button class="ui button green">Verder Winkelen</button>
                        </div>
                    <?php } ?>
            </div>
            <div class="sixteen wide column">
<!--                TODO: Showing random products-->
            </div>
        </div>
    </div>
<!--    --><?php //include_once 'footer.php'; ?>
    <?php include_once 'base/scripts.php'; ?>
</content>

</body>
</html>