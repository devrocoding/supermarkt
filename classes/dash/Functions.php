<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 09:49
 */

class functions
{

    public $short = 'q=';

    public function serialize($data)
    {
        return $this->base64_url_encode($data);
    }

    public function getHomeData()
    {
        return '?' . $this->short . $this->serialize('index');
    }

    public function translateRole($role)
    {
        switch ($role){
            case 'ADMIN':
                return "Administrator";
                break;
            case 'USER':
                return "Gebruiker";
                break;
            case 'GUEST':
                return "Guest";
                break;
        }
    }

    public function gotoPage($page, $path = null)
    {
        $path_url = '';

        $url = $this->base64_url_encode($page);
        $query = $_GET;
        $query[str_replace('=', '', $this->short)] = $url;
        if (is_array($path)) {
            foreach ($path as $p) {
                $path_url .= '/'.$p;
            }
            $query['path'] = $this->base64_url_encode($path_url);
        }else{
            unset($query['path']);
        }
        $query_result = http_build_query($query);
        return $_SERVER['PHP_SELF'] . '?' . $query_result;
    }

    public function getPageName()
    {
        $path = './';
        $page = $this->base64_url_decode($_GET[str_replace('=', '', $this->short)]);
        $org_page =  $path . '/' . $page . ".php";
//        $page = str_replace(str_split('.php.//'), '', $org_page);
        $f = str_replace('_', ' ', $page);
        $final = '';
        foreach (explode(' ', $f) as $str){
            $final .= ucfirst($str) . ' ';
        }
        return $final;
    }

    public function getPageFromCurrentUrl(){
        if (isset($_GET[str_replace('=', '', $this->short)])) {
            $path = './';
            if (isset($_GET['path'])){
                $path = '.' . $this->base64_url_decode($_GET['path']);
            }
            $page = $this->base64_url_decode($_GET[str_replace('=', '', $this->short)]);
            return $path . '/' . $page . ".php";
        }
    }

    public function readPage($page){
        return include_once $page;
    }

    function base64_url_encode($input) {
        return strtr(base64_encode($input), '+/=', '._-');
    }

    function base64_url_decode($input) {
        return base64_decode(strtr($input, '._-', '+/='));
    }

}