<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 12:44
 */

require_once 'DB.php';

class user extends DB
{

    public function isLoggedIn()
    {
        return isset($_SESSION['email']) && !empty($_SESSION['email']);
    }

    public function getRole()
    {
        return $_SESSION['role'];
    }

    public function isLoggedInAsGuest()
    {
        return isset($_SESSION['guest']);
    }

    public function getEmail()
    {
        return $_SESSION['email'];
    }

    public function getName()
    {
        return isset($_SESSION['name']) ? $_SESSION['name'] : $_SESSION['email'];
    }

    public function isAdmin()
    {
        return $this->getRole() == 'ADMIN';
    }

    public function getAllUsers()
    {
        $this->connect();
        $this->select('customer', '*');
        $this->disconnect();
        return $this->getResult();
    }

    public function selectUser($id){
        $this->connect();
        $this->select('customer', '*', null, 'id=' . $id);
        $this->disconnect();
        return $this->getResult();
    }

    public function selectUserByEmail($email){
        $this->connect();
        $this->select('customer', '*', null, "email='" . $email . "'");
        $this->disconnect();
        return $this->getResult();
    }
}