<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 20-12-2017
 * Time: 11:10
 */

require_once 'Shop.php';

class product extends Shop
{

    public function selectProduct($id)
    {
        $this->connect();
        $this->select('products', '*', null, 'id=' . $id);
        $this->disconnect();
    }

    public function getPriceWithoutVat($result)
    {
        if ($result == null){
            $result = $this->getResult()[0];
        }

        return $result['price'];
    }

    public function getPrice($result)
    {
        if ($result == null){
            $result = $this->getResult()[0];
        }

        $price = $this->getPriceWithoutVat($result);

        return $price;
    }

    public function getVATOfProduct($result)
    {
        if ($result == null){
            $result = $this->getResult()[0];
        }

        return $this->getPriceWithoutVat($result) / 100 * $this->formatVAT($result['vat']);
    }

    public function getDescription($result)
    {
        if ($result == null){
            $result = $this->getResult()[0];
        }

        return $result['desc'];
    }

    public function isNew($date)
    {
        $now = date('Y-m-d');
        $subDate = strtotime('-30 day', strtotime($now));
        return strtotime($date) >= ($subDate);
    }

}