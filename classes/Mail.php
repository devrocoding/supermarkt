<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 15-1-2018
 * Time: 14:04
 */

require '../vendor/autoload.php';

class Mail
{

    public function send($customer_email, $user_data, $order_id)
    {
        $order = new order();
        try {
            $mail = new PHPMailer\PHPMailer\PHPMailer();

            $mail->isSMTP();
            $mail->SMTPDebug = 4;

            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;
            $mail->Host = 'ssl://smtp.gmail.com:465';

            $mail->Username = "devrocoding@gmail.com";
            $mail->Password = "jucian0808";
            $mail->SMTPAuth = true;

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->WordWrap = 50;
            $mail->isHTML(true);
            $mail->SMTPDebug = true;
            $mail->do_debug = 2;
            $mail->SMTPSecure = 'ssl';

            //Recipients
            $mail->setFrom('support@devfood.nl', 'DevFood B.V');
            $mail->addAddress($customer_email);
            $mail->addReplyTo('support@devfood.nl', 'Support');

            //Attachments
//            $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            $url = 'http://localhost/PHP_STORM/supermarkt/dashboard/index.php?q=b3JkZXJz';

            $mail->isHTML(true);
            $mail->Subject = 'Bestelling #' . $order->getOrderID($order_id) . ' is geplaatst';
            $mail->Body    = 'Geachte ' . $user_data['name'] . ',<br><br>
                              Uw bestelling met nummer ' . $order->getOrderID($order_id) . ' is met goede order ontvangen!<br>
                              Wij zullen zo snel mogelijk uw bestelling verzenden en hem afgeven bij de pakket dienst!<br><br>
                              Mocht u een factuur willen uitprinten? Die kunt u vinden in uw <a href="'.$url.'">Dashboard</a><br><br>
                              Met vriendelijke groet,<br><br>
                              Justin<br>
                              Directeur Devfood B.V';


            $mail->send();
            echo 'Message has been sent';
        }catch (Exception $e){
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }
    }

}