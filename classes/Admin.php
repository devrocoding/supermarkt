<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 30-11-2017
 * Time: 11:13
 */
require_once 'DB.php';

class Admin extends DB
{

    public function isLoggedIn()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        return isset($_SESSION['email']) && !empty($_SESSION['email']);
    }

    public function showName()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        return isset($_SESSION['name']) ? $_SESSION['name'] : $_SESSION['email'];
    }

    public function signOut()
    {
        if ($this->isLoggedIn()) {
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            session_unset();
            session_destroy();
            header("Refresh");
        }
    }

    public function getAllAdmins()
    {
        $this->connect();
        $this->select('administrator', '*');
        $this->disconnect();
        return $this->getResult();
    }

}