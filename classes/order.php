<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 20-12-2017
 * Time: 11:24
 */

require_once 'DB.php';

class order extends DB
{

    public function selectOrderByUser($order_id = null)
    {
        $customer_id = $_SESSION['id'];
        $this->connect();
        if ($order_id == null) {
            $this->select('orders', '*', '', 'customer_id=' . $customer_id);
        }else{
            $this->select('orders', '*', '', 'customer_id=' . $customer_id . ' AND order_id=' . $order_id);
        }
        $this->disconnect();
    }

    public function getResultOrder()
    {
        return $this->getResult();
    }

    public function getOrderID($order)
    {
        return str_pad($order, 5, '0', STR_PAD_LEFT);
    }

    public function hasOrder()
    {
        return $this->numRows() > 0;
    }

    public function getTotalPrice($order)
    {
        //TODO: Count all products in order
        return number_format($order, 2);
    }

    public function getListOfProducts($id, $amount = 1)
    {
        $products = $this->getProductsByOrder($id);
        $final = array($amount);
        $i = 0;
        foreach ($products as $res){
            if ($i >= $amount){
                break;
            }
            $final[$i] = $res;
            $i++;
        }
        return $final;
    }

    public function getVATFromProducts($products)
    {
        $price = 0.00;

        foreach ($products as $product){
            $vat = $this->formatVAT(intval($product['vat']));
            $pPrice = $product['price'];
            $price += ($pPrice / 100 * $vat) * $product['amount'];
        }

        return $price;
    }

    public function getShippingPrice($products)
    {
        return (doubleval($this->getSubTotal($products)) < 15.00 ? 4.95 : 0.00);
    }

    public function getTotal($products)
    {
        return $this->getSubTotal($products) + $this->getShippingPrice($products);
    }

    public function getSubTotal($products)
    {
        $price = 0;
        if ($products != null) {
            foreach ($products as $pr) {
                $price += $pr['price'] * $pr['amount'];
            }
        }
        return $price;
    }

    public function formatVAT($vat = 0)
    {
        return ($vat == 1 ? $this->getHighVAT() : $this->getLowVAT());
    }

    public function getHighVAT()
    {
        return 21;
    }

    public function getLowVAT()
    {
        return 6;
    }

    public function placeOrder()
    {
        $this->connect();
        $this->select('orders');

        $order_id = $this->numRows() + 1;
        $customer_id = $_SESSION['id'];
        $date = date("Y-m-d");

        $products = $_SESSION['cart'];

        $this->insert('orders', array("order_id" => $order_id, "customer_id" => $customer_id, "order_date" => $date));

        foreach ($products as $product){
            $this->insert('product_orders', array("order_id" => $order_id, "product_id" => $product['id'], "amount" => $product['amount']));
        }

        $this->disconnect();
        return $order_id;
    }

    public function getProductsByOrder($id)
    {
        $this->connect();
        $this->select('product_orders', '*', 'orders ON product_orders.order_id = orders.order_id
                                                               INNER JOIN products ON product_orders.product_id = products.id', 'product_orders.order_id=' . $id);
        $this->disconnect();
        return $this->getResult();
    }

}