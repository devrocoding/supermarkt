<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 13-12-2017
 * Time: 13:26
 */
require_once 'DB.php';

class Shop extends DB
{

    public function getTotalPriceCart()
    {
        //TODO: Tijdelijk
        $price = 0;
        if ($this->getProductsInCart() != null) {
            foreach ($this->getProductsInCart() as $pr) {
                $price += $pr['price'];
            }
        }
        return $price;
    }

    public function hasAShoppingCart()
    {
        return count($this->getProductsInCart()) > 0;
    }

    public function getVat()
    {
        $price = 0.00;

        foreach ($_SESSION['cart'] as $product){
            $vat = $this->formatVAT(intval($product['vat']));
            $pPrice = $product['price'];
            $price += ($pPrice / 100 * $vat);
        }

        return number_format($price, 2, ',', '.');
    }

    public function formatVAT($vat = 0)
    {
        return ($vat == 1 ? $this->getHighVAT() : $this->getLowVAT());
    }

    public function getTotal()
    {
        return number_format($this->getTotalPriceCart() + $this->getShippingPrice(), 2, ',', '.');
    }

    public function getProductsInCart()
    {
        if (isset($_SESSION['cart'])){
            return $_SESSION['cart'];
        }else{
            return null;
        }
    }

    public function getShippingPrice()
    {
        return (doubleval($this->getTotalPriceCart()) < 15.00 ? 4.95 : 0.00);
    }

    public function getProductsFromDB($filter = null)
    {
        $this->connect();
        if ($filter == null) {
            $this->select('products', '*');
        }else{
            $this->select('products', '*', null, "title LIKE '%" . $filter . "%'");
        }
        $this->disconnect();
        return $this->getResult();
    }

    private function getProductsByCato($filter = null, $cato = null, $date = false)
    {
        $this->connect();

        $query = '';

        if ($cato != null)
            $query .= "category = '" . $cato . "'";

        if ($filter != null) {
            if ($cato != null)
                $query .= " AND ";
            $query .= "title LIKE '%" . $filter . "%'";
        }

        $this->select('products', '*', null, $query);

//        echo $this->numRows();
        $this->disconnect();
        return $this->getResult();
    }

    public function getQueryForCato($filter, $page){
        switch ($page){
            case "ALLES": return $this->getProductsByCato($filter);
                break;
            case "NEW": return $this->getAllNewProducts($filter);
                break;
            DEFAULT: return $this->getProductsByCato($filter, $page);
        }
    }

    public function isNew($date)
    {
        $now = date('Y-m-d');
        $subDate = strtotime('-30 day', strtotime($now));
        return strtotime($date) >= ($subDate);
    }

    public function getAllNewProducts($filter = null)
    {
        $array = array();
        $int = 0;
        $result = $this->getProductsFromDB($filter);
        foreach ($result as $p){
            if ($this->isNew($p['added'])) {
                $array[$int] = $p;
            }
            $int++;
        }
        return $array;
    }

    public function getNumberOfAllProducts($filter = null)
    {
        $this->connect();
        if ($filter == null) {
            $this->select('products', '*');
        }else{
            $this->select('products', '*', null, "title LIKE '%" . $filter . "%'");
        }
        $amount = $this->numRows();
        $this->disconnect();
        return $amount;
    }

    public function getHighVAT()
    {
        return 21;
    }

    public function getLowVAT()
    {
        return 6;
    }

}