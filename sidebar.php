<?php
    //session_start();
    define("ABS_PATH", dirname(__FILE__));

    require_once ABS_PATH . './classes/User.php';
    require_once ABS_PATH . './classes/dash/Functions.php';
    require_once ABS_PATH . './classes/Shop.php';
    $user = new User();
    $shop = new Shop();
    $functions = new Functions();
?>
<!--Top Menu-->
<div class="ui top fixed menu" id="top-main-menu">
    <div class="item">
        <img class="ui small image" src="./assets/img/logo.png" id="top-main-menu">
    </div>
    <a class="item" href='./'>
        <i class="home icon"></i> Home
    </a>
    <a class="item" href="./shop">
        <i class="shopping bag icon"></i> Webshop
    </a>
<!--    <a class="item">-->
<!--        <i class="mail icon"></i> Blog-->
<!--    </a>-->
<!--    <a class="item">-->
<!--        <i class="info icon"></i> Over Ons-->
<!--    </a>-->
    <div class="right menu">
        <a class="item orange" href="shopping_cart.php">
            <i class="shop icon"></i>€<?php echo number_format($shop->getTotalPriceCart(), '2', ',', '.'); ?>
        </a>
        <div class="item">
            <?php if ($user->isLoggedIn()){ ?>
            <a class="ui green image large label" id="topbar-label" href="<?php
                if ($user->isAdmin()){
                    echo './dashboard/admin';
                }else{
                    echo './dashboard';
                }
            ?>">
                Dashboard
            </a>
            <a class="ui red label" id="sign-out">
                <div class="ui active mini inline inverted loader sign-out" style="display: none"></div>
                Uitloggen
            </a>
            <?php }else{ ?>
                <a class="ui green label" id="login">Klanten Login</a>
                <?php include_once 'login_popup.php'; ?>
            <?php } ?>
        </div>
    </div>

<!--    Are you sure logout modal-->
    <div class="ui mini test modal sign-out">
        <div class="header">
            <h1>Weet je het zeker?</h1>
        </div>
        <div class="content">
            <div class="ui grid">
                <div class="sixteen wide column center aligned">
                    <i class="icon warning sign massive grey center aligned"></i>
                </div>
            </div>

        </div>
        <div class="actions">
            <div class="ui negative button">
                Nee
            </div>
            <div class="ui positive right labeled icon button">
                Ja
                <i class="checkmark icon"></i>
            </div>
        </div>
    </div>
<!--    End of modal logout-->

</div>
<!--End of top menu-->