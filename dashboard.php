<?php
require_once '/classes/Admin.php';
require_once '/classes/DB.php';
require_once '/classes/dash/Functions.php';

$admin = new Admin();
$functions = new Functions();
?>

<html>
<head>
    <?php include("base/imports.php"); ?>
</head>
<body>
<?php
if (!isset($_GET[str_replace('=', '', $functions->short)])) {
    header("Location:". $_SERVER['PHP_SELF']  . $functions->getHomeData());
}

if (!$admin->isLoggedIn()){
    header("Location: login_popup.php");
}
?>

<content>
<?php include_once 'sidebar.php'; ?>
    <div class="ui container">
        <?php $functions->readPage($functions->getPageFromCurrentUrl()); ?>
    </div>
<?php include_once 'base/scripts.php'; ?>
</content>
</body>
</html>