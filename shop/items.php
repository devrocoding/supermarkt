<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 21-12-2017
 * Time: 14:52
 */
require_once '../classes/Shop.php';
require_once '../classes/Order.php';
require_once '../classes/product.php';
$order = new Order();
$shop = new Shop();
$product = new product();
$functions = new Functions();

$filter = (isset($_SESSION['FILTER_PRODUCT']) ? (($_SESSION['FILTER_PRODUCT'] != null || $_SESSION['FILTER_PRODUCT'] != '') ? $_SESSION['FILTER_PRODUCT'] : null) : null);
$page = strtoupper($functions->getPageName());
if (strpos($page, 'ALLE') !== false){
    $page = "ALLES";
}else if (strpos($page, 'NIEUW') !== false){
    $page = "NEW";
}

//echo $page;
$array = $shop->getQueryForCato($filter, $page);

//$functions->readPage($functions->getPageFromCurrentUrl());

?>

<div class="ui four cards basic segment" id="product-card">
    <?php foreach ($array as $p){?>
    <div class="ui card">
        <div class="image">
            <?php if ($p['image'] == null){ ?>
                <img src="./assets/img/no_img.png">
            <?php }else{ ?>
                <img src="./assets/img/products/<?php echo $p['image']; ?>">
            <?php } ?>
            <?php if($product->isNew($p['added'])){ ?>
                <a class="ui teal left ribbon label">NIEUW</a>
            <?php } ?>
        </div>
        <div class="content">
            <i class="right floated like icon"></i>
            <i class="right floated star icon"></i>
            <a class="header"><?php echo $p['title']; ?></a>
            <div class="meta">
                <span class="date">
<!--                TODO: Categorien laten zien-->
                    500 Gram
                </span>
            </div>
            <div class="description">
                <?php echo $p['desc']; ?>
            </div>
    </div>
        <div class="extra content">
            <div class="center aligned">
                <h3>
                    <i class="euro icon"></i>
                    <?php echo $product->getPrice($p); ?>
                </h3>
            </div>
        </div>
        <?php
                if (isset($_SESSION['cart']) && isset($_SESSION['cart'][$p['id']])){ ?>
                    <div class="ui bottom attached red button add_to_cart" product_id="<?php echo $p['id'];?>" add_to_cart="false">
                        <i class="remove icon"></i>
                        Verwijder
                    </div>
                <?php }else{ ?>
                    <div class="ui bottom attached green button add_to_cart" product_id="<?php echo $p['id'];?>" add_to_cart="true">
                        <i class="add icon"></i>
                        Voeg toe
                    </div>
                <?php } ?>
    </div>
    <?php } ?>
</div>
