<?php
session_start();
require_once '../classes/dash/Functions.php';
require_once '../classes/Admin.php';
require_once '../classes/Shop.php';

$functions = new Functions();
$admin = new Admin();

if (!isset($_GET[str_replace('=', '', $functions->short)])) {
    header("Location:". $_SERVER['PHP_SELF'] . $functions->gotoPage('aanbiedingen', array('cato')));
}
?>
<html>
<head>
    <?php include("../base/imports.php"); ?>
</head>
<body>

<content>
    <?php include_once '../sidebar.php'; ?>
    <div class="ui fluid container">
        <div class="ui grid">
            <div class="sixteen wide column">
                <h2 class="ui center aligned icon header dividing purple">
                    <?php echo $functions->getPageName(); ?>
                    <div class="sub header">
                        Bekijk onze uitgebreide assortiment van producten
                    </div>
                </h2>
                <div class="ui grid">
                    <div class="four wide column"></div>
                    <div class="ten wide column center aligned" style="margin-top: 3em;">
                        <div class="ui menu">
                            <div class="item">
                                <div class="ui icon input">
                                    <input type="text" placeholder="Zoek een product..." id="product-search" value="<?php
                                    echo (isset($_SESSION['FILTER_PRODUCT']) ? (($_SESSION['FILTER_PRODUCT'] != null || $_SESSION['FILTER_PRODUCT'] != '') ? $_SESSION['FILTER_PRODUCT'] : null) : null); ?>">
                                    <i class="search icon"></i>
                                </div>
                            </div>
<!--                            <div class="right item">-->
<!---->
<!--                                <div class="ui floating labeled icon dropdown button">-->
<!--                                    <i class="filter icon"></i>-->
<!--                                    <span class="text">Sorteer op</span>-->
<!--                                    <div class="menu">-->
<!--                                        <div class="header">-->
<!--                                            Waarop sorteren?-->
<!--                                        </div>-->
<!--                                        <div class="ui left icon input">-->
<!--                                            <i class="search icon"></i>-->
<!--                                            <input type="text" name="search" placeholder="Search...">-->
<!--                                        </div>-->
<!--                                        <!--                                        <div class="header">-->
<!--                                        <!--                                            <i class="tags icon"></i>-->
<!--                                        <!--                                            Meest Populair-->
<!--                                        <!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="ui purple empty circular label"></div>-->
<!--                                            Meest populair-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="ui red empty circular label"></div>-->
<!--                                            Hoge prijs eerst-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="ui blue empty circular label"></div>-->
<!--                                            Lage prijs eerst-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="ui black empty circular label"></div>-->
<!--                                            Nieuw-->
<!--                                        </div>-->
<!--                                        <div class="item">-->
<!--                                            <div class="ui orange empty circular label"></div>-->
<!--                                            Aanbieding-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->

                        </div>

                    </div>
                    <div class="two wide column"></div>
                    <div class="two wide column">
                    </div>
                    <div class="two wide column">
                        <?php include_once 'filter_bar.php'; ?>
                    </div>
                    <div class="ten wide column">
                        <div id="product-items">
                            <?php include_once 'items.php'; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php include_once '../base/scripts.php'; ?>
</content>
<script>
    $('.ui.dropdown')
        .dropdown()
    ;
</script>
</body>
</html>