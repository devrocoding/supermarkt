<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 21-12-2017
 * Time: 14:11
 */

require_once '../classes/dash/Functions.php';
require_once '../classes/Shop.php';
$functions = new Functions();
$shop = new Shop();

?>

<div class="ui vertical pointing menu">
    <a class="item" href="<?php echo $functions->gotoPage('alle_producten', array('cato')); ?>">
        Alle Producten
        <div class="ui red horizontal label"><?php echo $shop->getNumberOfAllProducts(); ?></div>
    </a>
    <a class="item" href="<?php echo $functions->gotoPage('aanbiedingen', array('cato')); ?>">
        Aanbiedingen
        <div class="ui red horizontal label"><?php echo $shop->getNumberOfAllProducts(); ?></div>
    </a>
    <a class="item" href="<?php echo $functions->gotoPage('pasen', array('cato')); ?>">
        Pasen
    </a>
    <a class="item" href="<?php echo $functions->gotoPage('nieuw', array('cato')); ?>">
        Nieuw
        <div class="ui green horizontal label">+<?php echo sizeof($shop->getAllNewProducts()); ?></div>
    </a>
    <a class="item" href="<?php echo $functions->gotoPage('benodigdheden', array('cato')); ?>">
        Benodigdheden
    </a>
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('groente_&_fruit', array('cato')); ?><!--">-->
<!--        Groente & Fruit-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('vlees_&_vis', array('cato')); ?><!--">-->
<!--        Vlees & Vis-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('kant_&_klaar', array('cato')); ?><!--">-->
<!--        Kant & Klaar-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('zuivel', array('cato')); ?><!--">-->
<!--        Zuivel-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('brood_&_beleg', array('cato')); ?><!--">-->
<!--        Brood & Beleg-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('voorraadkast', array('cato')); ?><!--">-->
<!--        Voorraadkast-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('snoep_&_snacks', array('cato')); ?><!--">-->
<!--        Snoep & Snacks-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('dranken', array('cato')); ?><!--">-->
<!--        Dranken-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('diepvries', array('cato')); ?><!--">-->
<!--        Diepvries-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('mens_&_dier', array('cato')); ?><!--">-->
<!--        Mens & Dier-->
<!--    </a>-->
<!--    <a class="item" href="--><?php //echo $functions->gotoPage('kruiden', array('cato')); ?><!--">-->
<!--        Kruiden-->
<!--    </a>-->
<!--    <div class="item" id="huishouden">-->
<!--        Huishouden-->
<!--    </div>-->

</div>

<!--<div class="ui vertical pointing menu">-->
<!--    <div class="item">-->
<!--        <div class="menu">-->
<!--            <a class="item">-->
<!--                Aanbiedingen-->
<!--                <div class="ui red mini horizontal label">24</div>-->
<!--            </a>-->
<!--            <a class="item">-->
<!--                <b>Kerst</b>-->
<!--                <div class="ui green mini horizontal label">354</div>-->
<!--            </a>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="item">-->
<!--        <div class="header">Products</div>-->
<!--        <div class="menu">-->
<!--            <a class="item">Home</a>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="item">-->
<!--        <div class="header">Products</div>-->
<!--        <div class="menu">-->
<!--            <a class="item">Home</a>-->
<!--        </div>-->
<!--    </div><div class="item">-->
<!--        <div class="header">Products</div>-->
<!--        <div class="menu">-->
<!--            <a class="item">Home</a>-->
<!--        </div>-->
<!--    </div><div class="item">-->
<!--        <div class="header">Products</div>-->
<!--        <div class="menu">-->
<!--            <a class="item">Home</a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->