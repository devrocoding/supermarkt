<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 19:17
 */

require_once '../classes/Admin.php';
require_once '../classes/DB.php';

$admin = new Admin();
$db = new DB();

$can_log = false;
if ($admin->isLoggedIn()) {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        var_dump($_POST);
        $db->connect();
        if (isset($_POST['safe_sign_out'])) {
            $admin->signOut();
            echo json_encode("SUCCES");
        }else

            /** PRODUCTS */

            if (isset($_POST['add_product'])) {
                $title = $_POST['title'];
                $desc = $_POST['description'];
                $price = $_POST['price'];
                $image = $_POST['image'];
                $vat = intval($_POST['vat']);

                var_dump($_FILES);

                $db->insert('products', array('id' => null, 'title' => $title, 'desc' => $desc, 'price' => $price, 'vat' => $vat, 'added' => date('Y-m-d'), 'image' => $image));

            echo json_encode("SUCCES");
        }else

            /** ADMINS */

            if (isset($_POST['add_admin'])) {
                $title = $_POST['email'];

                $db->select('administrator', '*', null, "email='" . $title . "'");
                if ($db->numRows()>0){
                    echo 'ALREADY_EXISTS';
                    return;
                }else {

                    $desc = hash('sha512', $_POST['password']);
                    $price = $_POST['name'];

                    $db->insert('administrator', array('id' => null, 'email' => $title, 'password' => $desc, 'name' => $price, 'role' => 'ADMIN'));

                    echo json_encode("SUCCES");

                }
            }else

            /** CUSTOMER */

            if (isset($_POST['add_customer'])) {
                $title = $_POST['email'];

                $db->select('customer', '*', null, "email='" . $title . "'");
                if ($db->numRows() > 0) {
                    echo 'ALREADY_EXISTS';
                    return;
                } else {
                    $password = md5($_POST['password']);

                    $name = $_POST['firstname'] . ' ' . $_POST['between_name'] . ' ' . $_POST['lastname'];
                    $house_number = $_POST['house_number'] . ' ' . $_POST['extra_house_number'];

                    $db->insert('customer', array('id' => null, 'name' => $name, 'email' => $title, 'password' => $password, 'street' => $_POST['street'],
                        'house_number' => $house_number, 'zip' => $_POST['zip'], 'city' => $_POST['city'], 'phone' => $_POST['phone'], 'mobile' => $_POST['mobile'],
                        'gender' => $_POST['gender'], 'birth' => $_POST['birth'], 'birth_land' => $_POST['birth_land'], 'role' => $_POST['role']));

                    echo json_encode("SUCCES");

                }
            }

        $db->disconnect();
    }
}