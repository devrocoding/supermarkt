<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 22-12-2017
 * Time: 09:04
 */

session_start();
require_once '../classes/Admin.php';
require_once '../classes/DB.php';
require_once '../classes/product.php';

$admin = new Admin();
$db = new DB();
$product = new Product();


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $db->connect();
    if (isset($_POST['add_to_cart'])) {
        if ($_POST['add_to_cart'] == 'true') {
            $id = intval($_POST['id']);
            $db->select('products', '*', null, 'id=' . $id);
            $result = $db->getResult()[0];

            $_SESSION['cart'][$id] = array('id' => $id, 'price' => $product->getPrice($result), 'vat' => $result['vat'],'image' => $result['image'] ,'title' => $result['title'], 'desc' => $result['desc'], "amount" => 1);

            echo 'SUCCES';
        }else if ($_POST['add_to_cart'] == 'false'){
            $id = intval($_POST['id']);
            unset($_SESSION['cart'][$id]);
            echo 'SUCCES';
        }else
            if (isset($_POST['add_customer'])) {
                $title = $_POST['email'];

                $db->select('customer', '*', null, "email='" . $title . "'");
                if ($db->numRows() > 0) {
                    echo 'ALREADY_EXISTS';
                    return;
                } else {
                    $password = md5($_POST['password']);

                    $db->insert('customer', array('id' => null, 'name' => null, 'email' => $title, 'password' => $password, 'street' => null,
                        'house_number' => null, 'zip' => null, 'city' => null, 'phone' => 0, 'mobile' => 0,
                        'gender' => null, 'birth' => null, 'birth_land' => null, 'role' => null));

                    echo json_encode("SUCCES");

                }
            }
    }else if (isset($_POST['FILTER'])) {
        $_SESSION['FILTER_PRODUCT'] = $_POST['FILTER'];
        echo $_POST['FILTER'];
    }
//    var_dump($_SESSION);
    $db->disconnect();
}