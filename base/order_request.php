<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 10-1-2018
 * Time: 13:43
 */

session_start();
require_once '../classes/Admin.php';
require_once '../classes/User.php';
require_once '../classes/DB.php';
require_once '../classes/order.php';
require_once '../classes/dash/Functions.php';
require_once '../classes/Shop.php';
require_once '../classes/Mail.php';

$db = new DB();
$admin = new Admin();
$order = new order();
$user = new User();
$func = new Functions();
$shop = new Shop();
$mail = new Mail();

$can_log = false;
//if (!$admin->isLoggedIn()){
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['ORDER_STEP_1'])) {
        $db->connect();
        if ($admin->isLoggedIn()){
            $name = $_POST['firstname'] . ' ' . $_POST['between_name'] . ' ' . $_POST['lastname'];
            $house_number = $_POST['house_number'] . ' ' . $_POST['extra_house_number'];

            $db->update('customer', array("name" => $name, "street" => $_POST['street'], "house_number" => $house_number, "zip" => $_POST['zip'], "city" => $_POST['city'],
                "email" => $_POST['email']),
                'id='.$_SESSION['id']);
        }else{
            $title = $_POST['email'];

            $db->select('customer', '*', null, "email='" . $title . "'");
            if ($db->numRows() > 0) {
                echo 'ALREADY_EXISTS';
                return;
            } else {
                $password = md5($_POST['password']);

                $name = $_POST['firstname'] . ' ' . $_POST['between_name'] . ' ' . $_POST['lastname'];
                $house_number = $_POST['house_number'] . ' ' . $_POST['extra_house_number'];

                $db->insert('customer', array('id' => null, 'name' => $name, 'email' => $_POST['email'], 'password' => $password, 'street' => $_POST['street'],
                    'house_number' => $house_number, 'zip' => $_POST['zip'], 'city' => $_POST['city'], 'phone' => 0, 'mobile' => 0,
                    'gender' => null, 'birth' => null, 'birth_land' => $_POST['birth_land'], 'role' => 'CUSTOMER'));

                $result = $user->selectUserByEmail($_POST['email']);

                $_SESSION['email'] = $_POST['email'];
                $_SESSION['role'] = 'CUSTOMER';
                $_SESSION['id'] = $result[0]['id'];

                if (isset($result[0]['name'])) {
                    $_SESSION['name'] = $_POST['name'];
                }
            }
        }

        if (!isset($_SESSION['ORDER_STEP'])) {
            $_SESSION['ORDER_STEP'] = 2;
        }
//        $_SESSION['order_page_url'] = $func->serialize('overzicht');

        $id = $order->placeOrder();

        $_SESSION['order_id_order'] = $id;

        echo json_encode("SUCCES");

        $result = $user->selectUserByEmail($_POST['email']);
        $mail->send($_POST['email'], $result[0], $id);

        $_SESSION['order_succeed'] = $id;
        unset($_SESSION['cart']);
        unset($_SESSION['ORDER_STEP']);
        unset($_SESSION['order_page_url']);
        $db->disconnect();
    }else if (isset($_POST['GET_INVOICE_DATA'])) {
        $order_id = $_POST['id'];
        $order->selectOrderByUser($order_id);
        $orders = $order->getResultOrder();

        $orderDate = $orders[0]['order_date'];
        $products = $order->getProductsByOrder($order_id);
        $user = $user->selectUser($_SESSION['id']);
        $shoppingCart = array("subTotal" => $order->getSubTotal($products), "total" => $order->getTotal($products), "shippingPrice" => $order->getShippingPrice($products), "vatPrice" => $order->getVATFromProducts($products));

        echo json_encode(array('orderDate' => $orderDate, 'products' => $products, 'user' => $user, 'order_id' => $order_id, 'cart' => $shoppingCart));
    }
}