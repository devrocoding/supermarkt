<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 14-11-2017
 * Time: 20:49
 */

?>
<base href="http://localhost/PHP_STORM/supermarkt/" />
<!--Meta Attr.-->
<meta name="description" content="Een overzicht van projecten die tijdens mijn opleiding zijn gemaakt." />
<!--End of Meta Attr,-->

<!--Semantic-->
<link rel="stylesheet" type="text/css" href="semantic/dist/semantic.min.css">
<!---------->

<!--Intern Files-->
<link rel="stylesheet" href="./assets/css/style.css">
<!---------->