<?php

session_start();

?>
<html>
<head>
    <?php include("base/imports.php"); ?>
</head>
<body>

<content>
    <?php include_once 'sidebar.php'; ?>
<!--    <div class="ui fluid-container">-->
        <div class="ui grid">
            <div class="sixteen wide column">
                <!-- Page Contents -->
                <div class="pusher">
                    <div class="ui inverted vertical masthead center aligned segment" style="background: url('assets/img/dan-gold-298710.jpg'); background-size: 1920px">

                        <div class="ui text container">
                            <h1 class="ui inverted header">
                                Imagine-a-Company
                            </h1>
                            <h2>Do whatever you want when you want to.</h2>
                            <a class="ui huge primary button" href="shop">Begin met shoppen <i class="right arrow icon"></i></a>
                        </div>

                    </div>

                    <div class="ui vertical stripe segment">
                        <div class="ui middle aligned stackable grid container">
                            <div class="row">
                                <div class="eight wide column">
                                    <h3 class="ui header">We helpen mensen aan hun eten</h3>
                                    <h3 class="ui header">We helpen arme mensen in nood</h3>
                                </div>
                                <div class="six wide right floated column">
                                    <img src="assets/img/1049.png" class="ui large bordered rounded image">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="ui vertical stripe quote segment">
                        <div class="ui equal width stackable internally celled grid">
                            <div class="center aligned row">
                                <div class="column">
                                    <h3>"Wat een bedrijf"</h3>
                                    <p>Dit is alles wat je nodig hebt</p>
                                </div>
                                <div class="column">
                                    <h3>"Je zou haast niet geloven dat dit bestond"</h3>
                                    <p>
                                        <img src="assets/img/john-small.png" class="ui avatar image"> <b>Justin</b> Geweldige support en vooral lekker eten
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <?php include_once 'base/scripts.php'; ?>
</content>

</body>
</html>