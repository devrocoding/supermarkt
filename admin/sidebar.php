<?php
require_once './classes/User.php';
require_once './classes/dash/Functions.php';
$user = new User();
$functions = new Functions();
?>
    <!--Top Menu-->
    <div class="ui top fixed menu inverted" id="top-main-menu">
        <div class="item">
            <img class="ui small image" src="./assets/img/logo.png" id="top-main-menu">
        </div>
        <a class="item" href='dashboard.php<?php echo $functions->getHomeData(); ?>'>
            <i class="home icon"></i> Home
        </a>
        <a class="item" href="<?php echo $functions->gotoPage('portfolio'); ?>">
            <i class="grid layout icon"></i> Portfolio
        </a>
        <a class="item">
            <i class="mail icon"></i> Comments
        </a>
        <div class="ui simple dropdown item">
            More
            <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item"><i class="edit icon"></i> Edit Profile</a>
                <a class="item"><i class="globe icon"></i> Choose Language</a>
                <a class="item"><i class="settings icon"></i> Account Settings</a>
            </div>
        </div>
        <div class="right menu">
            <div class="item">
                <a class="ui yellow image large label" id="topbar-label">
                    <!--                <img src="./assets/img/steve.jpg">-->
                    <?php echo $user->getName(); ?>
                    <div class="detail"><?php echo $functions->translateRole($user->getRole()); ?></div>
                </a>
                <a class="ui red label" id="sign-out">Sign Out</a>
            </div>
        </div>

    </div>
    <!--End of top menu-->

<?php if ($user->isAdmin()){ ?>
    <!--Side (Left) main menu-->
    <div class="ui left fixed vertical menu inverted" id="left-main-menu">
        <div class="menu-item">
            <div class="item">
                <div class="header">Administrators</div>
                <div class="menu">
                    <a class="item">Toevoegen</a>
                    <a class="item">Lijst</a>
                    <a class="item">Rollen</a>
                </div>
            </div>
            <div class="item">
                <div class="header">Projecten</div>
                <div class="menu">
                    <a class="item">Toevoegen</a>
                    <a class="item">Lijst</a>
                </div>
            </div>
            <div class="item">
                <div class="header">Extra</div>
                <div class="menu">
                    <a class="item">Settings</a>
                    <a class="item">Statitics</a>
                </div>
            </div>
        </div>
    </div>
    <!--End of side left menu-->
<?php } ?>