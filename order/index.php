<?php
session_start();
require_once '../classes/dash/Functions.php';
require_once '../classes/Admin.php';
require_once '../classes/Shop.php';
require_once '../classes/User.php';
require_once '../classes/order.php';

$functions = new Functions();
$shop = new Shop();
$admin = new Admin();

$step = 0;
$page_url;
if (isset($_SESSION['order_page_url'])){
    $page_url = $_SERVER['PHP_SELF'] . '?' . $functions->short . $_SESSION['order_page_url'];
}else {
    $page_url = $_SERVER['PHP_SELF'] . '?' . $functions->short . $functions->serialize('gegevens');
}

if (!isset($_GET[str_replace('=', '', $functions->short)])) {
    header("Location:". $page_url);
}
if ($_SESSION['order_succeed']){
    header('location:./'+$functions->gotoPage('overzicht'));
    unset($_SESSION['order_succeed']);
}else
if ($shop->getProductsInCart() == null){
    header('location:../shopping_cart.php');
}

if (isset($_SESSION['ORDER_STEP']))
    $step = $_SESSION['ORDER_STEP'];

?>
<html>
<head>
    <?php include("../base/imports.php"); ?>
</head>
<body>

<content>
    <?php include_once '../sidebar.php'; ?>
    <div class="ui container">
        <div class="ui grid">
            <div class="sixteen wide column">
                <h2 class="ui center aligned icon header dividing teal">
                    <?php echo $functions->getPageName(); ?>
<!--                    <div class="sub header">-->
<!--                    </div>-->
                </h2>
                <div class="ui grid">
                    <div class="four wide column">
                        <div class="ui hidden divider"></div>
                        <div class="ui vertical steps" style="width: 100%">
<!--                            <a class="completed step">-->
                            <div class="<?php echo $step == 0 ? 'active' : 'completed'?> step">
                                <i class="book icon"></i>
                                <div class="content">
                                    <div class="title">Gegevens</div>
<!--                                    <div class="description"></div>-->
                                </div>
                            </div>
<!--                            <a class="active step">-->
<!--                            --><?php //echo $step; ?>
<!--                            href="--><?php //echo $functions->gotoPage('betaling', null); ?><!--"-->
                            <div class="<?php echo $step == 1 ? 'active' : ($step == 2 ? 'completed' : 'disabled'); ?> step">
                                <i class="payment icon"></i>
                                <div class="content">
                                    <div class="title">Betaling</div>
<!--                                    <div class="description"></div>-->
                                </div>
                            </div>
                            <div class="<?php echo $step == 2 ? 'active' : 'disabled'; ?> step">
                                <i class="info icon"></i>
                                <div class="content">
                                    <div class="title">Overzicht</div>
<!--                                    <div class="description">Description</div>-->
                                </div>
                            </div>
                        </div>
                        <div class="ui left piled floated segment" style="width: 100%">
                            <table class="ui very basic collapsing celled table">
                                <thead>
                                <tr><th colspan="3">
                                        Winkelwagentje
                                    </th>
                                </tr></thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Aantal Items (<?php echo sizeof($shop->getProductsInCart()); ?>)
                                    </td>
                                    <td class="left aligned">€ <?php echo $shop->getTotalPriceCart(); ?></td>
                                </tr>
                                <?php if ($shop->hasAShoppingCart()){ ?>
                                <tr>
                                    <td>
                                        Verzendkosten
                                    </td>
                                    <td class="left aligned">€ <?php echo $shop->getShippingPrice(); ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        Nog te betalen
                                    </td>
                                    <td class="left aligned">€ <?php echo $shop->getTotal(); ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="twelve wide column">
                        <?php $functions->readPage($functions->getPageFromCurrentUrl()); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once '../base/scripts.php'; ?>
</content>

</body>
</html>