<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 15-1-2018
 * Time: 09:51
 */

$order = new order();
unset($_SESSION['order_succeed']);
?>


<div class="ui container">
    <div class="ui grid">
        <div class="sixteen wide column">
            <div class="ui icon success message">
                <i class="send icon"></i>
                <div class="content">
                    <div class="header">GELUKT!</div>
                    <p>Je bestelling met nummer #<?php echo $order->getOrderID($_SESSION['order_id_order']); ?></p>
                    <p>Bekijk je bestelling in je dashboard, en print je factuur uit.</p>
                </div>
            </div>
        </div>
    </div>
</div>

