<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 20-12-2017
 * Time: 10:38
 */

require_once '../classes/Order.php';

$order = new Order();

?>

<?php
$order->selectOrderByUser();
if ($order->hasOrder()){
?>

<table class="ui selectable orange table">
    <thead>
    <tr><th>Order ID</th>
        <th>Prijs</th>
        <th>Datum</th>
        <th>Bestelde Producten</th>
        <th></th>
    </tr></thead><tbody>

    <?php foreach ($order->getResult() as $res){ ?>

    <tr>
        <td>#<?php echo $order->getOrderID($res['order_id']); ?></td>
        <td>€ <?php echo $order->getSubTotal($order->getProductsByOrder($res['order_id'])); ?></td>
        <td><?php $date = new DateTime($res['order_date']); echo $date->format('d M, Y'); ?></td>
        <td>
            <?php foreach($order->getListOfProducts($res['order_id'], 3) as $pro) { ?>
                    <div class="ui label">
                        <?php echo $pro['title']; ?>
                        <div class="detail">x<?php echo $pro['amount']; ?></div>
                    </div>
            <?php } ?>
            <?php if (sizeof($order->getProductsByOrder($res['order_id']))-3 > 0){ ?>
<!--                TODO: Show modal with more produtcs in tabel-->
            <a class="ui label green">
                Nog
                <div class="detail"><?php echo sizeof($order->getProductsByOrder($res['order_id']))-3; ?></div>
            </a>
            <?php } ?>
        </td>
        <td><a class="ui button blue get_invoice" data-id="<?php echo $res['order_id']; ?>"><i class="download icon"></i>Factuur</a></td>
    </tr>
    <?php } ?>
    </tbody>
</table>

<?php } else { ?>

    <div class="ui icon error message">
        <i class="attention circle icon"></i>
        <div class="content">
            <div class="header">Je hebt nog niets besteld!</div>
            <p>Om je bestellingen te kunnen bekijken, moet je eerst iets besteld hebben!</p>
        </div>
    </div>

<?php } ?>
