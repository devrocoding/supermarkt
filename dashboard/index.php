<?php
require_once '../classes/dash/Functions.php';
require_once '../classes/Admin.php';

$functions = new Functions();
$admin = new Admin();

if (!$admin->isLoggedIn()){
    header("Location:../");
}

if (!isset($_GET[str_replace('=', '', $functions->short)])) {
    header("Location:". $_SERVER['PHP_SELF'] . '?' . $functions->short . $functions->serialize('statitics'));
}

?>
<html>
<head>
    <?php include("../base/imports.php"); ?>
</head>
<body>

<content>
    <?php include_once '../sidebar.php'; ?>
    <div class="ui container">
        <div class="ui grid">
            <div class="sixteen wide column">
                <h2 class="ui center aligned icon header dividing teal">
                    <i class="circular users icon"></i>
                    <?php echo $functions->getPageName(); ?>
                    <div class="sub header">
                        Check out our plug-in marketplace
                    </div>
                </h2>
                <div class="ui grid">
                    <div class="four wide column">
                        <?php $functions->readPage('floating_bar.php'); ?>
                    </div>
                    <div class="twelve wide column">
                        <?php $functions->readPage($functions->getPageFromCurrentUrl()); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php include_once '../base/scripts.php'; ?>
</content>

</body>
</html>