<?php

require_once '../classes/DB.php';

$db = new DB();

$db->connect();
$db->select('customer', '*', null, 'id=' . $_SESSION['id']);
$result = $db->getResult()[0];

$name = explode(' ', $result['name']);
$house_number = explode(' ', $result['house_number']);

?>

<div class="ui stacked  segment">
    <h1 class="ui header center aligned dividing grey">Verander je account<br></h1>
    <form class="ui form" id="add_customer">
        <div class="fields">
            <div class="seven wide field">
                <label>Voornaam</label>
                <input type="text" name="firstname" placeholder="Voornaam" value="<?php echo (trim($name[0]) == '' ? '' : $name[0]); ?>">
            </div>
            <div class="three wide field">
                <label>Tussenvoegsel</label>
                <input type="text" name="between_name" placeholder="" value="<?php echo (trim($name[1]) == '' ? '' : $name[1]); ?>">
            </div>
            <div class="six wide field">
                <label>Achternaam</label>
                <input type="text" name="lastname" placeholder="Achternaam" value="<?php echo (trim($name[2]) == '' ? '' : $name[2]); ?>">
            </div>
        </div>
        <div class="field">
            <label>Email</label>
            <div class="ui right labeled input">
                <label for="email" class="ui label">@</label>
                <input type="text" id="email" name="email" placeholder="example@example.com" value="<?php echo $result['email']; ?>">
            </div>
        </div>
        <div class="fields">
            <div class="ten wide field">
                <label>Straat</label>
                <input type="text" name="street" placeholder="Straat" value="<?php echo $result['street']; ?>">
            </div>
            <div class="three wide field">
                <label>Huisnummer</label>
                <input type="text" name="house_number" placeholder="Huisnummer" value="<?php echo (trim($house_number[0]) == '' ? '' : $house_number[0]); ?>">
            </div>
            <div class="three wide field">
                <label>Toevoeging</label>
                <input type="text" name="extra_house_number" placeholder="" value="<?php echo (trim($house_number[1]) == '' ? '' : $house_number[1]); ?>">
            </div>
        </div>
        <div class="fields">
            <div class="five wide field">
                <label>Postcode</label>
                <input type="text" name="zip" placeholder="Postcode" value="<?php echo $result['zip']; ?>">
            </div>
            <div class="eleven wide field">
                <label>Woonplaats</label>
                <input type="text" name="city" placeholder="Stad" value="<?php echo $result['city']; ?>">
            </div>
        </div>
        <div class="two fields wide">
            <div class="field">
                <label>Telefoon</label>
                <div class="ui labeled input">
                    <div class="ui label">
                        +31
                    </div>
                    <input type="text" placeholder="612345678" name="phone" value="<?php echo ($result['phone'] == 0 ? '' : $result['phone']); ?>">
                </div>
            </div>
            <div class="field">
                <label>Mobiel</label>
                <div class="ui labeled input">
                    <div class="ui label">
                        +31
                    </div>
                    <input type="text" placeholder="612345678" name="mobile" value="<?php echo ($result['mobile'] == 0 ? '' : $result['mobile']); ?>">
                </div>
            </div>
        </div>
        <div class="field">
            <label>Verander je wachtwoord</label>
            <div class="ui right labeled input">
                <label for="password" class="ui label"><i class="lock icon"></i></label>
                <input type="password" name="password" placeholder="Wachtwoord">
            </div>
        </div>
        <div class="field">
            <div class="ui right labeled input">
                <label for="password" class="ui label"><i class="lock icon"></i></label>
                <input type="password" name="crypt_password" placeholder="Wachtwoord Opnieuw">
            </div>
        </div>
        <button class="ui button blue fluid" type="submit" name="add_customer">Veranderen</button>
        <div class="ui error message"></div>
    </form>
</div>

<?php $db->disconnect(); ?>