<?php
session_start();
require_once '../../classes/User.php';
require_once '../../classes/dash/Functions.php';
require_once '../../classes/Shop.php';
$user = new User();
$shop = new Shop();
$functions = new Functions();

include_once '../../sidebar.php';

?>



<!--Side (Left) main menu-->
<div class="ui left fixed vertical menu inverted" id="left-main-menu">
    <div class="menu-item">
<!--        <div class="item">-->
<!--            <div class="header">Administrators</div>-->
<!--            <div class="menu">-->
<!--                <a class="item" href="--><?php //echo $functions->gotoPage('roles', array('administrator')); ?><!--">Rollen</a>-->
<!--<!--                <a class="item" href="-->--><?php ////echo $functions->gotoPage('filter', array('administrator')); ?><!--<!--">Lijst</a>-->-->
<!--            </div>-->
<!--        </div>-->
        <div class="item">
            <div class="header">Klanten</div>
            <div class="menu">
                <a class="item" href="<?php echo $functions->gotoPage('add', array('customer')); ?>">Toevoegen</a>
<!--                <a class="item" href="--><?php //echo $functions->gotoPage('orders', array('extra')); ?><!--">Bestellingen</a>-->
                <a class="item" href="<?php echo $functions->gotoPage('filter', array('customer')); ?>">Lijst</a>
            </div>
        </div>
        <div class="item">
            <div class="header">Producten</div>
            <div class="menu">
                <a class="item" href="<?php echo $functions->gotoPage('add', array('product')); ?>">Toevoegen</a>
                <a class="item" href="<?php echo $functions->gotoPage('filter', array('product')); ?>">Lijst</a>
            </div>
        </div>
<!--        <div class="item">-->
<!--            <div class="header">Extra</div>-->
<!--            <div class="menu">-->
<!--                <a class="item" href="--><?php //echo $functions->gotoPage('stats', array('extra')); ?><!--">Statitics</a>-->
<!--            </div>-->
<!--        </div>-->
    </div>
</div>
<!--End of side left menu-->