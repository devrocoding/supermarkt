<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 14-12-2017
 * Time: 20:26
 */?>
<h2 class="ui center aligned icon header">
    <i class="circular plus icon"></i>
    Voeg een administrator toe
</h2>
<div class="ui negative message hidden add-admin-error-message">
    <div class="header">
        Er ging iets mis met het aanmaken van een administrator
    </div>
    <p>Of de velden waren niet correct, of de administrator bestaat al!</p>
</div>
<div class="ui positive message hidden add-admin-done-message">
    <div class="header">
        <i class="icon checkmark"></i>GELUKT! De administrator is aangemaakt
    </div>
</div>
<div class="ui piled segment">
    <form class="ui form" id="add_admin">
        <div class="field">
            <label>Naam</label>
            <input type="text" name="name" placeholder="Naam">
        </div>
        <div class="field">
            <label>Email</label>
            <input type="text" name="email" placeholder="Email">
        </div>
        <div class="field">
            <div class="ui right labeled input">
                <label for="password" class="ui label"><i class="lock icon"></i></label>
                <input type="password" name="password" placeholder="Wachtwoord">
            </div>
        </div>
        <div class="field">
            <div class="ui right labeled input">
                <label for="password" class="ui label"><i class="lock icon"></i></label>
                <input type="password" name="crypt_password" placeholder="Wachtwoord Opnieuw">
            </div>
        </div>
        <button class="ui button blue fluid" type="submit">Toevoegen</button>
        <div class="ui error message"></div>
    </form>
</div>
