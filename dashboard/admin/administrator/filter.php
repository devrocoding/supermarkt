<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 14-12-2017
 * Time: 20:26
 */

require_once '../../classes/Admin.php';
$admin = new Admin();

$admins = $admin->getAllAdmins();
if ($admin->numRows() > 0){ ?>

    <table class="ui celled padded table">
        <thead>
        <tr><th class="single line">Naam</th>
            <th>Email</th>
            <th>Rol</th>
            <th></th>
        </tr></thead>
        <tbody>
        <?php

        foreach ($admins as $p) { ?>

            <tr>
                <td class="single line">
                    <h3><?php echo $p['name']; ?></h3>
                </td>
                <td class="single line">
                    <h3><?php echo $p['email']; ?></h3>
                </td>
                <td><?php echo $p['role']; ?></td>
                <td class="center aligned">
                    <button class="ui icon red button">
                        <i class="trash large icon"></i>
                    </button>
                </td>
            </tr>

        <?php } ?>

        </tbody>
        <tfoot>
        <tr><th colspan="4">
                <div class="ui left floated pagination menu">
                    <a class="icon item">
                        <i class="left chevron icon"></i>
                    </a>
                    <a class="item">1</a>
                    <a class="item">2</a>
                    <a class="item">3</a>
                    <a class="item">4</a>
                    <a class="icon item">
                        <i class="right chevron icon"></i>
                    </a>
                </div>
            </th>
        </tr></tfoot>
    </table>

<?php }else{ ?>

    <div class="ui icon error message">
        <i class="attention circle icon"></i>
        <div class="content">
            <div class="header">Er zijn geen administrators</div>
            <p>Dit betekent dat er nog geen admins bestaan of er ging ergens iets mis.</p>
        </div>
    </div>

<?php } ?>
