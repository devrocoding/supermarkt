<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 14-12-2017
 * Time: 20:26
 */

$shop = new Shop();

?>
<h2 class="ui center aligned icon header">
    <i class="circular plus icon"></i>
    Voeg een product toe
</h2>
<div class="ui negative message hidden add-product-error-message">
    <div class="header">
        Er ging iets mis met het aanmaken van het product.
    </div>
    <p>Controleer de velden, anders raadpleeg de beheerder!</p>
</div>
<div class="ui positive message hidden add-product-done-message">
    <div class="header">
        <i class="icon checkmark"></i>GELUKT! Je product is aangemaakt
    </div>
</div>
<div class="ui piled segment">
    <form class="ui form" id="add_product" enctype="multipart/form-data">
        <div class="field">
            <label>Titel</label>
            <input type="text" name="title" placeholder="Title">
        </div>
        <div class="field">
            <label>Korte Beschrijving</label>
            <textarea rows="2" name="description"></textarea>
        </div>
        <div class="field">
            <div class="ui right labeled input">
                <label for="amount" class="ui label">€</label>
                <input type="text" placeholder="Price" name="price" id="price" value="0.00">
            </div>
        </div>
        <div class="field">
            <label>BTW Status</label>
            <select class="ui dropdown" name="vat">
                <option value="">Huidige BTW status</option>
                <option value="1">Hoog Tarief (<?php echo $shop->getHighVAT(); ?>%)</option>
                <option value="0">Laag Tarief (<?php echo $shop->getLowVAT(); ?>%)</option>
            </select>
        </div>

        <div class="field">
            <label for="amount">Select image</label>
            <select class="ui dropdown" name="image">
                <option value="">Selecteer afbeelding</option>
                <?php
                $dirname = "../../assets/img/products/";
                $images = glob($dirname."/*.{jpg,png,gif}", GLOB_BRACE);

                foreach($images as $image) {
                    echo '<option value="'.basename($image).'"><i class="image icon"></i>'.basename($image).'</option>';
                }

                ?>
            </select>
        </div>

        <br>
        <button class="ui button blue fluid" type="submit" name="add_product">Submit</button>
        <div class="ui error message"></div>
    </form>
</div>
