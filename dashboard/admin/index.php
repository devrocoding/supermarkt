<?php
require_once '../../classes/Admin.php';
require_once '../../classes/DB.php';
require_once '../../classes/dash/Functions.php';

$admin = new Admin();
$functions = new Functions();

if (!$admin->isLoggedIn()){
    header("Location:../../");
}

if (!isset($_GET[str_replace('=', '', $functions->short)])) {
    header("Location:". $_SERVER['PHP_SELF']  . $functions->getHomeData());
}

?>

<html>
<head>
    <?php include("../../base/imports.php"); ?>
</head>
<body>

<content>
    <?php include_once 'sidebar.php'; ?>

    <div class="ui container" id="dashboard">
        <div class="ui grid">
            <div class="sixteen wide column">
                <?php $functions->readPage($functions->getPageFromCurrentUrl()); ?>
            </div>
        </div>
    </div>

    <?php include_once '../../base/scripts.php'; ?>
    <script src="./assets/js/dashboard.js"></script>
</content>

</body>
</html>