<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 20-12-2017
 * Time: 10:38
 */?>

<div class="ui statistics center aligned">
    <div class="statistic">
        <div class="value">
            <?php echo 2; ?>
        </div>
        <div class="label">
            Bestelling(en)
        </div>
    </div>
    <div class="statistic">
        <div class="text value">
            Honderd<br>
            Euro
        </div>
        <div class="label">
            Uitgegeven
        </div>
    </div>
    <div class="statistic">
        <div class="value">
            <i class="lock icon"></i> 5
        </div>
        <div class="label">
            Logins
        </div>
    </div>
</div>
