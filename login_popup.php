<div class="ui modal login">
    <i class="close icon"></i>
    <div class="column">
        <h2 class="ui teal image header">
            <div class="content center aligned">
                <h1 class="modal-title">LOGIN</h1>
            </div>
        </h2>
    </div>
    <div class="ui two column stackable center aligned grid segment">
        <div class="sixteen wide column">
            <div class="ui negative message hidden login-form-error-message">
                <div class="header">
                    Wij kunnen geen account vinden! Probeer het opnieuw
                </div>
                <p>Geen account? Je kan je ook <a href="#">Registreren</a></p>
            </div>
            <form class="ui large form" id="login-form">
                <div class="ui stacked segment">
                    <div class="field">
                        <div class="ui left icon login input">
                            <i class="user icon"></i>
                            <input type="text" name="email" id="login-input" placeholder="E-mail address">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon login input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" id="password_login" placeholder="Wachtwoord">
                        </div>
                    </div>
                    <div class="ui fluid large teal submit login button" id="login-submit">Login</div>
                </div>
                <div class="ui error message"></div>
            </form>
        </div>
<!--        <div class="ui vertical divider" style="margin: 10px">or</div>-->
<!--        <div class="column">-->
<!--            <form class="ui large form" id="register-form">-->
<!--                <div class="ui stacked segment">-->
<!--                    <div class="field">-->
<!--                        <div class="ui left icon register input">-->
<!--                            <i class="user icon"></i>-->
<!--                            <input type="text" name="email" id="register-input" placeholder="E-mail address">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="field">-->
<!--                        <div class="ui left icon register input">-->
<!--                            <i class="lock icon"></i>-->
<!--                            <input type="password" name="password" id="password_register" placeholder="Wachtwoord">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="field">-->
<!--                        <div class="ui left icon input">-->
<!--                            <i class="lock icon"></i>-->
<!--                            <input type="password" name="crypt_password" id="crypt_password_register" placeholder="Wachtwoord Opnieuw">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="ui fluid large teal submit register button" id="register-submit">Registreren</div>-->
<!--                </div>-->
<!--                <div class="ui error message"></div>-->
<!--            </form>-->
<!--        </div>-->
    </div>
</div>